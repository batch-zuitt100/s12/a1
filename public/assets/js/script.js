//Activity 1

function numTester (num) {
	if (num % 3 == 0 && num % 5 == 0) {
		return "FizzBuzz";
	} else if (num % 3 == 0) {
		return "Fizz";
	} else if (num % 5 == 0) {
		return "Buzz";
	} else {
		return "Pop";
	}
}

console.log(numTester(5)); //Buzz
console.log(numTester(30)); //FizzBuzz
console.log(numTester(27)); //Fizz
console.log(numTester(17)); //Pop



//Activity 2

function letterTester (letter) {
	let letterCleaned = letter.toLowerCase();
	if (letterCleaned == "r") {
		return "Red";
	} else if (letterCleaned == "o") {
		return "Orange";
	} else if (letterCleaned == "y") {
		return "Yellow";
	} else if (letterCleaned == "g") {
		return "Green";
	} else if (letterCleaned == "b") {
		return "Blue";
	} else if (letterCleaned == "i") {
		return "Indigo";
	} else if (letterCleaned == "v") {
		return "Violet";
	} else {
		return "No color";
	}
}


console.log(letterTester("r")); //Red
console.log(letterTester("G")); //Green
console.log(letterTester("x")); //No color
console.log(letterTester("B")); //Blue
console.log(letterTester("i")); //Indigo



//Activity 3
//Every year that is exactly divisible by four is a leap year, except for years that are exactly divisible by 100, but these centurial years are leap years if they are exactly divisible by 400. For example, the years 1700, 1800, and 1900 are not leap years, but the years 1600 and 2000 are.[11]

function yearTester (year) {
	if (year % 4 == 0 && year % 100 !== 0 || year % 400 == 0) {
		return "Leap year";
	} else {
		return "Not a leap year";
	}
}

console.log(yearTester(1900)); //Not a leap year
console.log(yearTester(2000)); //Leap year
console.log(yearTester(2004)); //Leap year
console.log(yearTester(2021)); //Not a leap year